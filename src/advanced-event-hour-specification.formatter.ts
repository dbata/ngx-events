// tslint:disable: no-bitwise
import { AdvancedColumnFormatter } from '@universis/ngx-tables';
import {DatePipe} from '@angular/common';
import {ConfigurationService} from '@universis/common';
import { DateFormatter } from './advanced-date.formatter';


export class EventHourSpecificationFormatter extends AdvancedColumnFormatter {
  static formatDays(daysString: string, dayFormatter = (day: number) => String(day), groupSize = 3) {
    // Return all days
    if (!daysString || daysString === '*') { 
      return `${dayFormatter(1)}-${dayFormatter(7)}`
    }

    const days = daysString.replace(/ /g, '').split(',').map(d => parseInt(d)).sort((a, b) => a - b);

    let groupedDays = [];
    let start = 0;

    while (start < days.length) {
      let end = start + 1;
      let consecutiveDays = 0;

      // Find consecutive days
      while (end < days.length && days[end] - days[end - 1] === 1) {
        consecutiveDays++;
        end++;
      }

      if (consecutiveDays >= groupSize - 1) {
        // Add grouped range
        groupedDays.push(`${dayFormatter(days[start])}-${dayFormatter(days[start + consecutiveDays])}`);
        start += consecutiveDays + 1;
      } else {
        // Add individual day
        groupedDays.push(dayFormatter(days[start]));
        start++;
      }
    }

    return groupedDays.join(",");
  }

  render(data, type, row, meta) {
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      const datePipe: DatePipe = new DatePipe(this.injector.get(ConfigurationService).currentLocale);
      const dayFormatter = (day: number): string => datePipe.transform(new Date(0, 0, day), 'EEEEEE');
      const timeFormatter = (time: Date): string => datePipe.transform(time, 'HH:mm');
      const days = EventHourSpecificationFormatter.formatDays(data, dayFormatter);
      const time = timeFormatter(row.opens) + ' - ' + timeFormatter(row.closes);
      const dotColor = DateFormatter.stringToColor(row.name);
      return `<span class="px-1" style="height: 13px; width: 13px; background-color: ${dotColor}; border-radius: 50%; display: inline-block;"></span>
        <span class="pl-2">${days}</span><span> ${time}</span>`;
    }
    return data;
  }

  constructor() {
    super();
  }
}
